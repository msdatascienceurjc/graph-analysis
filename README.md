# Graph Analysis

Analyzing Unity project with Perceval, NetworkX and D3.js

## First steps

**Install Perceval package to run unity_git_data.py**

pip3 install perceval

**Create an ubuntu instance in AWS**

ssh -i Downloads/LightsailDefaultKey-us-west-2.pem ubuntu@54.202.222.127

**Copy path**

scp -i Downloads/LightsailDefaultKey-us-west-2.pem ubuntu@54.202.222.127:data.json data.json

**Once in your machine, find your tmux session**

tmux list-sessions

**Join tmux session (in this example is called grafo) and run the script. "grafo" is the name I gave to my session**

tmux attach -t grafo
